# 前言 


代码编写规范，主要包括两部分，**代码风格**和**最佳工程实践**。

- 在代码风格上，没有一种代码编写风格是最好的，更重要的是与已有项目代码风格保持一致，以提高项目团队整体对代码的可读性。

- 在工程实践上，统一一些开发流程，提升团队的协作效率，另外就是最佳工程实践规范，以提高代码的性能、可靠性以及可读性。



## 为什么要有规范

编码规范对于程序员而言尤为重要，有以下几个原因：

- 一个软件的生命周期中，80%的花费在于维护

- 几乎没有任何一个软件，在其整个生命周期中，均由最初的开发人员来维护

- 编码规范可以改善软件的可读性，可以让程序员尽快而彻底地理解新的代码

- 如果你将源码作为产品发布，就需要确任它是否被很好的打包并且清晰无误，一如你已构建的其它任何产品

## 编码规范原则

本文档中的准则致力于最大限度达到以下原则：

- 正确性

- 可读性

- 可维护性

- 可调试性

- 一致性

- 美观

尽管本文档涵盖了许多基础知识，但应注意的是，没有编码规范可以为我们回答所有问题，开发人员始终需要再编写完代码后，对上述原则做出正确的判断。


## 什么时候用 SHELL

## 用什么样的 SHELL

BASH 应该是唯一的 SHELL 脚本解释器。**因为绝大多数GNU/Linux默认安装的Shell都是bash，考虑到可移植性。SHELL脚本一般用BASH解释器来执行。**

对于 `#!/bin/bash` ，大多数写过脚本的人都知道，这是命令解释器的声明，通常位于脚本的第一行，它有个专业的名字，叫做 `shebang` ，作为对命令解释器的声明，`#!/bin/bash` 声明了 bash 程序所在的路径。

有了命令解释器的路径声明，那么当执行该脚本时，系统就知道该去哪里找这个命令解释器，也就是 Shebang 中指定的 bash 。

同理，`#!/bin/sh` 也是一样的，包括非 Shell 脚本，如 `#!/usr/bin/python` 也是同理，都是声明命令解释器的位置。

```
[opc@instance-20210621-0038 ~]$
[opc@instance-20210621-0038 ~]$ ls -al /bin/bash
-rwxr-xr-x. 1 root root 964536 Nov 22  2019 /bin/bash
[opc@instance-20210621-0038 ~]$
[opc@instance-20210621-0038 ~]$
[opc@instance-20210621-0038 ~]$ /bin/bash --version
GNU bash, version 4.2.46(2)-release (x86_64-redhat-linux-gnu)
Copyright (C) 2011 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>

This is free software; you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
[opc@instance-20210621-0038 ~]$
[opc@instance-20210621-0038 ~]$ ls -al /bin/sh
lrwxrwxrwx. 1 root root 4 May 13  2021 /bin/sh -> bash
[opc@instance-20210621-0038 ~]$
```


`#!/usr/bin/env bash` ，网上经常可以看到还有这种 `shebang` 写法

> env 命令用于显示系统中已存在的环境变量，以及在定义的环境中执行指令

这种 `shebang` 写法的原理是：**当执行 env bash 时，它其实会去操作系统的$PATH环境变量的路径中去查找bash可执行文件。**


- `#!/bin/bash` 是直接指定，把bash可执行文件的路径硬编码到`shebang`中

- `#!/usr/bin/env bash` 则是告诉系统去 $PATH 包含的目录中挨个去找吧，先找到哪个，就用哪个

### `#!/usr/bin/env bash` 优缺点

优点

- `#!/usr/bin/env bash` 不必在系统的特定位置查找命令解释器，为多系统间的移植提供了极大的灵活性和便利性（某些系统的一些命令解释器并不在 /bin 或 一些约定的目录下，而是一些比较奇怪的目录）

- 在不了解主机的环境时，`#!/usr/bin/env bash` 写法可以使开发工作快速地展开。


缺点

- `#!/usr/bin/env bash` 在对安全性比较看重时，该写法会出现安全隐患

    > `#!/usr/bin/env bash` 从 $PATH 中查找命令解释器所在的位置并匹配第一个找到的位置，这意味着可以伪造一个假的命令解释器（如自己写一个假的 bash），并将伪造后的命令解释器所在目录写入 PATH 环境变量中并位于靠前位置，这样，就形成了安全隐患。而 /bin 由于一般只有 root 用户才有操作权限，所以，#!/bin/bash 这种写法相对较为安全。

- `#!/usr/bin/env` 无法传递多个参数（这是由于 Shebang 解析造成的，并非 env 命令的缘故）

 > 如：
 #!/usr/bin/perl -w
#!/bin/csh -f
而如果使用 `#!/usr/bin/env perl -w` 这种写法的话，`perl -w` 会被当成一个参数，于是，根本找不到 perl -w 这个命令解释器，就会出错。

### `#!/bin/bash` 优缺点

优点

- 准确指出所需命令解释器的位置

- 安全性相对较高

- 可以传递多个参数

缺点

- 移植性相对较差，很多系统的命令解释器位置不一致

- 一些命令解释器的位置记不住

到底用哪个

- 两个都可以

- 如果对安全性比较看重，使用 #!/bin/bash

- 如果对安全性不是很看重，但对移植性（灵活性）比较看重，使用 #!/usr/bin/env bash

- 看自己的意愿，喜好





# 源文件


## 命名和编码

文件名使用小写字母加下划线的形式，且以.sh结尾。

函数名使用小写字母加下划线的形式，包名使用::。

包中使用小写字母驼峰形式。

变量名使用小写字母加下划线的形式，局部变量尽量使用local修饰，减少变量名冲突。

常量使用大写字母加下划线形式，并且添加readonly修饰。

**文件名命名规范**

可执行文件不建议有扩展名，库文件必须使用 .sh 作为扩展名，且应是不可执行的。

执行一个程序时，无需知道其编写语言，且shell脚本并不要求具有扩展名，所以更倾向可执行文件没有扩展名。

而库文件知道其编写语言十分重要，使用 .sh 作为特定语言后缀的扩展名，可以和其他语言编写的库文件加以区分。

文件名要求全部小写, 可以包含下划线 _ 或连字符 -, 建议可执行文件使用连字符，库文件使用下划线。

**文件编码规范**

源文件编码格式为UTF-8。 避免不同操作系统对文件换行处理的方式不同，一律使用LF。

**单行长度**

每行最多不超过120个字符。每行代码最大长度限制的根本原因是过长的行会导致阅读障碍，使得缩进失效。

除了以下两种情况例外：

- 导入模块语句

- 注释中包含的URL

如出现长度必须超过120个字符的字符串，应尽量使用here document或者嵌入的换行符等合适的方法使其变短。


## 注释

尽量使用代码自注释，即用代码名来表达清楚。无法表达清楚的使用注释。注释应说明设计思路而不是描述代码的行为，代码的行为尽量依赖代码本身来表述清楚。

```bash
# Delete a file in a sophisticated manner.


#######################################
# Get configuration directory.
# Globals:
#   SOMEDIR
# Arguments:
#   None
# Outputs:
#   Writes location to stdout
#######################################
get_dir() {
  echo "${SOMEDIR}"
}

#######################################
# Delete a file in a sophisticated manner.
# Arguments:
#  $1: File to delete, a path.
# Returns:
#   0 if thing was deleted, otherwise non-zero.
#######################################
del_thing() {
  rm "$1"
}
```


## 缩进

tab键设置为4个空格，默认缩进为4个空格。

```bash
main() {
    # 缩进4个空格
    say="hello World."
    echo "${say}"
}
```


## 函数

function定义，默认不需要加function修饰。函数统一放在源文件的全局变量之后，可执行代码之前，函数之间不放置可执行代码。代码功能比较少时，可以不定义main函数。

```bash
main() {
    echo "hello World."
    exit 0
}
```

## 命令执行失败需报错

实际上 Shell 脚本不像其他高级语言，如 Python, Ruby 等， Shell 脚本默认不提供安全机制，举个简单的例子， Ruby 脚本尝试去读取一个没有初始化的变量的内容的时候会报错，而 Shell 脚本默认不会有任何提示，只是简单地忽略。

```bash
#!/bin/bash

echo $v
echo "hello"

# output:
#
# hello
```

可以看到， echo $v 输出了一个空行， bash 完全忽略了不存在的 $v 继续执行后面的命令 echo "hello" 。这其实并不是开发者想要的行为，对于不存在的变量，脚本应该报错且停止执行来防止错误的叠加。好在，我们可以通过 set -u 来改变这种默认忽略未定义变量行为，脚本在头部加上它，遇到不存在的变量就会报错并停止执行。

```bash
#!/bin/bash
set -u

echo $a
echo bar

# output:
# ./script.sh: line 4: v: unbound variable
```

`set -u`命令用于设置shell选项，u是nounset表示当使用未定义的变量时，输出错误信息并强制退出。
所以`set -u`的另一种写法是`set -o nounset` 


## 命令执行失败需停止

对于默认的 Shell 脚本运行环境，有运行失败的命令（返回值非0）， bash 会继续执行后面的命令：

```
#!/bin/bash

unknowncmd
echo "hello"

# output:
# ./script.sh: line 3: unknowncmd: command not found
# hello

```

可以看到， bash 只是显示有错误，接着继续执行`shell`脚本，这种行为很不利于脚本安全和排错。实际开发中，如果某个命令失败，往往需要脚本停止执行，防止错误累积。这时，一般采用下面的写法：




## 多用trap捕捉信号


关于 shell 脚本另外一个常见的情况是，脚本执行失败导致文件系统处于不一致的状态，比如文件锁、临时文件或者 shell 脚本的错误只更新了部分文件。为了达到"事务的完整性"我们需要解决这些不一致的问题，要么删除文件锁、临时文件，要么将状态恢复到更新之前的状态。实际上， shell 脚本确实提供了一种在捕捉到特定的 unix 信号的情况下执行一段命令或者函数的功能：









## SUID/SGID




# 参考

https://blog.csdn.net/feihe0755/article/details/126484099