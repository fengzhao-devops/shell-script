# set命令

set 命令是 bash 脚本的重要环节，却常常被忽视，导致脚本的安全性和可维护性出问题。


```shell
# bash 执行脚本的时候，会创建一个新的 shell 进程
$ bash script.sh
```


上面代码中，script.sh是在一个新的 shell 里面执行。这个 shell 就是脚本的执行环境，bash 默认给定了这个环境的各种参数和环境变量

set命令用来修改 shell 环境的运行参数，也就是可以定制环境。一共有十几个参数可以定制，官方手册有[完整清单](https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html)




# set -u

执行脚本的时候，如果遇到不存在的变量，bash 默认忽略它，并继续执行后面的语句。

```shell
#!/usr/bin/env bash
echo $a
echo bar
```


对于上面这段脚本，$a是一个不存在的变量。执行结果如下。
```shell
$ bash script.sh

bar
```

可以看到，echo $a输出了一个空行，Bash 忽略了不存在的$a，然后继续执行echo bar。

大多数情况下，这不是开发者想要的行为，遇到变量不存在，脚本应该报错，而不是一声不响地往下执行。

set -u 就用来改变这种默认行为。在脚本头部加上它，遇到不存在的变量就会报错，并停止执行。

```
#!/usr/bin/env bash
set -u

echo $a
echo bar
```


运行这个脚本，执行到echo $a时就直接报错并中断，后面的语句都不会再被执行。set -u还有另一种写法 set -o nounset，两者是等价的。

```
#!/bin/bash
set -u 
# set -o nounset 等价于上面那行
```





