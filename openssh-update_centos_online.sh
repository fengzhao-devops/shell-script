#!/bin/bash
# @Link   :https://www.cnblogs.com/yanjieli/p/13445912.html
# reference https://fushengyicheng.com/p/centos7-rpm-%E7%A6%BB%E7%BA%BF%E5%8D%87%E7%BA%A7-openssh9-8/
set -e

echo 
echo -e "\033[40;31;1m*** 安装完成后请勿立即退出当前终端(断开连接)，先新开终端进行连接测试ok后再关闭该终端 ***\033[0m"
echo 
echo "即将升级openssh"
sleep 3

# Check if user is root
if [ $(id -u) != "0" ]; then
    echo "Error: You must be root to run this script!!"
    exit 1
fi

base_dir=`pwd`

#下载安装包：
openssh="openssh-8.8p1"
openssl="openssl-1.1.1l"


#Download the installation package
function download(){
    if [ ! -f ${openssh}.tar.gz ];then
        wget -c https://openbsd.hk/pub/OpenBSD/OpenSSH/portable/${openssh}.tar.gz -O ${openssh}.tar.gz
    else
        echo 'Skipping: openssh already downloaded'
    fi
    
    if [ ! -f ${openssl}.tar.gz ];then
        wget -c wget https://ftp.openssl.org/source/${openssl}.tar.gz -O ${openssl}.tar.gz
    else
        echo 'Skipping:  openssl already downloaded'
    fi
}


#安装依赖包
function install_relyon(){
    yum install -y telnet-server xinetd
    yum install  -y gcc gcc-c++ glibc make autoconf openssl openssl-devel pcre-devel  pam-devel
    yum install  -y pam* zlib*
    systemctl enable xinetd.service
    systemctl enable telnet.socket
    systemctl start telnet.socket
    systemctl start xinetd.service
    echo -e 'pts/0\npts/1\npts/2\npts/3'  >>/etc/securetty
    systemctl restart xinetd.service
    echo "telnet 启动成功"
    sleep 3
    echo "########################################################"
}


#备份ssh
function back_ssh(){
   rm -rf /tmp/ssh_backup/ && mkdir /tmp/ssh_backup/
   cp /root/.ssh/authorized_keys /tmp/ssh_backup/
   cp -r /etc/ssh/ /tmp/ssh_backup/
}


#安装openssl
function install_openssl(){
    rm -rf ./openssl && mkdir ./openssl && tar -xzvf ${base_dir}/${openssl}.tar.gz -C ./openssl --strip-components 1
    echo "备份OpenSSL..."
    mv -f /usr/bin/openssl /usr/bin/openssl_bak
    mv -f /usr/include/openssl /usr/include/openssl_bak
    mv -f /usr/lib64/libssl.so /usr/lib64/libssl.so.bak
    echo "开始安装OpenSSL..."
    sleep 3
    cd ${base_dir}/openssl
    ./config --prefix=/usr/local/openssl -d shared && make -j 4 && make install -j 4
    
    ln -fs /usr/local/openssl/bin/openssl /usr/bin/openssl
    ln -fs /usr/local/openssl/include/openssl /usr/include/openssl
    ln -fs /usr/local/openssl/lib/libssl.so /usr/lib64/libssl.so
    
    echo "加载动态库..."
    echo "/usr/local/openssl/lib" >> /etc/ld.so.conf
    /sbin/ldconfig
    echo "查看确认版本。。。"
    openssl version
    echo "OpenSSL 升级完成..."
}


#安装openssh
function install_openssh(){
    echo "开始升级OPENSSH。。。。。"
    sleep 5
    cd ${base_dir}
    #/usr/bin/tar -zxvf ${base_dir}/${openssh}.tar.gz
    rm -rf ./openssh && mkdir ./openssh && tar -xzvf ${base_dir}/${openssh}.tar.gz -C ./openssh --strip-components 1
    cd ${base_dir}/openssh
    chown -R root.root ${base_dir}/openssh

    # openssh8.8以下
    #./configure --prefix=/usr/local/openssh --sysconfdir=/etc/ssh  --with-openssl-includes=/usr/local/openssl/include \
    # --with-ssl-dir=/usr/local/openssl   --with-zlib   --with-md5-passwords   --with-pam  && make -j 4 && make install -j 4

    # openssh8.8及以上
    ./configure --prefix=/usr/ --sysconfdir=/etc/ssh  \
     --with-ssl-dir=/usr/local/openssl   --with-zlib   --with-md5-passwords   --with-pam
    
    if [ $? -eq 0 ]; then
      make
      make install

      echo "openssh 升级成功..."
      cd ${base_dir}/openssh
      # 从原先的解压的包中拷贝一些文件到目标位置、赋予执行权限、添加服务、开机启动
      cp -a contrib/redhat/sshd.init /etc/init.d/sshd
      cp -a contrib/redhat/sshd.pam /etc/pam.d/sshd.pam
    else
      echo "openssh 升级失败..."
      exit
    fi
}


# 配置ssh
function config_ssh(){
    chmod +x /etc/init.d/sshd
    chkconfig --add sshd
    chmod 600 /etc/ssh/ssh_host_ed25519_key
    chmod 600 /etc/ssh/ssh_host_rsa_key
    chmod 600 /etc/ssh/ssh_host_ecdsa_key
    systemctl enable sshd
    [ $? -eq 0 ] && echo "sshd服务添加为启动项 ..."
    mv -f /usr/lib/systemd/system/sshd.service  /tmp/
    #允许root远程登陆
    sed -i 's/#PermitRootLogin yes/PermitRootLogin yes/g' /etc/ssh/sshd_config
    chkconfig sshd on
    systemctl enable sshd
    /etc/init.d/sshd restart
    netstat -lntp
    echo "查看SSH版本信息。。。"
    ssh -V
    sleep 3
    #echo "telnet服务关闭..."
    #systemctl disable xinetd.service
    #systemctl stop xinetd.service
    #systemctl disable telnet.socket
    #systemctl stop telnet.socket
    echo "查看ssh服务"
    netstat -lntp
    echo "OpenSSH 版本升级成功................"
    sleep 3
}

function main(){
    download
    install_relyon
    back_ssh
    install_openssl
    install_openssh
    config_ssh
    exit
}

main


### sed -i 's/\r//g' openssh-update.sh