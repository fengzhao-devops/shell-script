#!/bin/bash
# filename      : /script/xtrabackup.sh
# Author        : fengzhao
# 备份策略： 每周日全备，周一周二增量备份，周三全备，周四周五周六增量备份，仅仅最近一周的备份文件
# 备份文件基路径 /qhdata/mysql_bak/


# 星期格式：0 1 2 3 4 5 6 
day=`date +%w`
# 日期格式：20200324
dt=`date +%Y%m%d`
# 前一天日期：20200323
lastday=`date -d '1 days ago' +%Y%m%d`
user=root
pwd='sj36668182'
socket=/data/mysql/mysql.sock
#socket=/qhdata/mysql/mysql.sock
# 备份文件基路径
base_dir=/data/mysql_bak

log=/data/mysql_bak/log/backuplog.`date +%Y%m%d`


case $day in  
    0)  
        # Sunday Full backup
        find $base_dir -name "xtra_*" -mtime +4 -exec rm -rf {} \;
        xtrabackup --defaults-file=/etc/my.cnf --backup --lock-ddl --user=$user --password=$pwd  --socket=$socket --no-timestamp  --target-dir=$base_dir/xtra_base_$dt > $log 2>&1
        ;;  
    1)  
        # Monday Relatively Sunday's incremental backup  
        xtrabackup --defaults-file=/etc/my.cnf --backup --lock-ddl --user=$user --password=$pwd  --socket=$socket --no-timestamp  --incremental  --target-dir=$base_dir/xtra_inc_$dt --incremental-basedir=$base_dir/xtra_base_$lastday > $log 2>&1  
        ;;  
    2)  
        # Tuesday Compared with Monday's incremental backup  
        xtrabackup --defaults-file=/etc/my.cnf --backup --user=$user --lock-ddl --password=$pwd --socket=$socket  --no-timestamp  --incremental  --target-dir=$base_dir/xtra_inc_$dt --incremental-basedir=/$base_dir/xtra_inc_$lastday > $log 2>&1     
        ;;  
    3)  
        # Wednesday Full backup
        # find $base_dir -name "xtra_*" -mtime +4 -exec rm -rf {} \;
        xtrabackup --defaults-file=/etc/my.cnf --backup --lock-ddl --user=$user --password=$pwd --socket=$socket --no-timestamp --target-dir=$base_dir/xtra_base_$dt > $log 2>&1   
        ;;  
    4)  
        # Thursday  Relatively Wednesday's incremental backup  
        xtrabackup --defaults-file=/etc/my.cnf --backup --user=$user --lock-ddl --password=$pwd --socket=$socket  --no-timestamp  --incremental  --target-dir=$base_dir/xtra_inc_$dt --incremental-basedir=$base_dir/xtra_base_$lastday > $log 2>&1    
        ;;  
    5)  
        # Friday Compared with Thursday's incremental backup  
        xtrabackup --defaults-file=/etc/my.cnf --backup --user=$user --lock-ddl --password=$pwd --socket=$socket --no-timestamp  --incremental  --target-dir=$base_dir/xtra_inc_$dt --incremental-basedir=$base_dir/xtra_inc_$lastday > $log 2>&1    
        ;;  
    6)  
        # Saturday Compared with Friday's incremental backup  
        xtrabackup --defaults-file=/etc/my.cnf --backup --user=$user --lock-ddl --password=$pwd --socket=$socket  --no-timestamp  --incremental  --target-dir=$base_dir/xtra_inc_$dt --incremental-basedir=$base_dir/xtra_inc_$lastday > $log 2>&1   
        ;;  
esac 


find /qhdata/mysql_bak/log/ -mtime +6 -type f -name 'backuplog.*' -exec rm -rf {} \;
